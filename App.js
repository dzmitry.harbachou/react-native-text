import 'react-native-gesture-handler';
import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';

import HomeScreen from './App/screens/HomeScreen';
import AccessibleRegistrationScreen from './App/screens/positive/ValidRegistrationScreen/RegistartionScreen';
import RegistrationScreen from './App/screens/negative/InValidRegistrationScreen/RegistartionScreen.js';

const Drawer = createDrawerNavigator();

const App: () => React$Node = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen
          name="AccessibleRegistrationForm"
          component={AccessibleRegistrationScreen}
        />
        <Drawer.Screen name="RegistrationForm" component={RegistrationScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default App;

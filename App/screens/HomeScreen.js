import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';

const imagePath =
  'https://miro.medium.com/max/1200/1*2KBZFhc6FXgg9IJZHEg-Ow.jpeg';

const HomeScreen = ({navigation}) => {
  return (
    <View style={styles.screen}>
      <ImageBackground style={styles.image} source={{uri: imagePath}} />
      <Text style={styles.text}>
        Welcome to the beta mobile application for testing accessibility
        features
      </Text>
      <View style={styles.box}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('AccessibleRegistrationForm')}>
          <Text style={styles.buttonText}>Accessible registration form</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => navigation.navigate('RegistrationForm')}>
          <Text style={styles.buttonText}>Registration form</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
  },
  box: {
    height: 100,
    justifyContent: 'space-between',
  },
  text: {
    color: '#fff',
    backgroundColor: 'rgba(52, 52, 52, 0.5)',
    borderRadius: 20,
    fontSize: 24,
    fontWeight: '700',
    width: 400,
    padding: 10,
    textAlign: 'center',
    marginTop: 200,
    marginBottom: 40,
  },
  image: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'orange',
    padding: 10,
    height: 48,
  },
  buttonText: {
    fontSize: 16,
    textTransform: 'uppercase',
    color: '#fff',
  },
});

export default HomeScreen;

import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  inValidTextStyles: {
    marginBottom: 40,
    backgroundColor: '#fff',
    color: '#dddddd',
  },
  input: {
    width: 350,
    height: 55,
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
  },
  editableText: {
    width: 40,
    height: 30,
    backgroundColor: '#42A5F5',
    margin: 10,
    padding: 8,
    color: 'white',
    borderRadius: 14,
    fontSize: 9,
    fontWeight: '500',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rotatedImage: {
    width: 110,
    height: 60,
    transform: [{translateY: 20}, {translateX: -70}, {rotate: '350deg'}],
  },
});

export default styles;

import React from 'react';
import {
  View,
  Text,
  Button,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';

import styles from './RegistartionScreen.styles';

const imagePath = 'https://imgshare.info/images/2019/01/11/ShowImage.md.png';

class InValidRegistrationForm extends React.Component {
  state = {
    username: '',
    password: '',
    email: '',
    phone_number: '',
    txt: 'Hello',
  };
  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.rotatedImage} source={{uri: imagePath}} />
        <Text
          style={styles.inValidTextStyles}
          onPress={() => {
            console.log('called');
          }}>
          This is clickable text
        </Text>
        <TextInput
          id=""
          style={styles.input}
          placeholder="Username"
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('username', val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('password', val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('email', val)}
        />
        <TextInput
          style={styles.input}
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('phone_number', val)}
        />
        <Button
          title=""
          onPress={() => {
            console.log('Registered');
          }}
        />
        <Text
          style={{marginBottom: 40}}
          onPress={() => {
            console.log('called');
          }}>
          I've already have account
        </Text>
        <Button
          title="I already have an account"
          onPress={() => {
            console.log('Registered');
          }}
        />
        <Text
          accessible={true}
          style={{marginBottom: 40}}
          onPress={() => {
            console.log('called');
          }}
        />
        <Text
          style={styles.editableText}
          onPress={() => this.setState({isEditing: true})}>
          {this.state.txt}
        </Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => console.log('test')}>
          <Text onPress={() => console.log('test')} style={styles.buttonText}>
            Registration form
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default InValidRegistrationForm;

import React from 'react';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';

import styles from './RegistartionScreen.styles';

class SignUp extends React.Component {
  state = {
    username: '',
    password: '',
    email: '',
    phone_number: '',
  };
  onChangeText = (key, val) => {
    this.setState({[key]: val});
  };

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          autoFocus={true}
          style={styles.input}
          placeholder="Username"
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('username', val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Password"
          secureTextEntry={true}
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('password', val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('email', val)}
        />
        <TextInput
          style={styles.input}
          placeholder="Phone Number"
          autoCapitalize="none"
          placeholderTextColor="white"
          onChangeText={val => this.onChangeText('phone_number', val)}
        />
        <TouchableOpacity
          accessible={true}
          accessibilityLabel="Sign Up"
          accessibilityHint="Press to create your own account"
          accessibilityRole="button"
          style={styles.button}
          onPress={() =>
            alert("You are welcome!!!, You've been successfully registered.", [
              {text: 'Continue', onPress: () => console.log('Ok')},
            ])
          }>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default SignUp;

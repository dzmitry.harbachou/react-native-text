import React, {Component} from 'react';
import {StyleSheet, Image, TouchableOpacity, Text, View} from 'react-native';

let SButton = () => {
  return (
    <TouchableOpacity onPress={this._onPressButton}>
      <Image style={styles.button} source={require('./myButton.png')} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
});

export default SButton;
